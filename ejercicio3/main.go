package main

import (
	"encoding/json"
	"log"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	_ "github.com/go-sql-driver/mysql"
)

type body struct {
	Message string `json:"message"`
}

// Handler is the Lambda function handler
//http://localhost:3000/?name=Jorge
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	log.Println("Lambda request", request.RequestContext.RequestID)
	name := request.QueryStringParameters["name"]

	data := struct {
		Greeting string
		Profile  string
	}{
		Greeting: "Hi " + name,
		Profile:  "Moderator",
	}

	b, _ := json.Marshal(data)

	return events.APIGatewayProxyResponse{
		Body:       string(b),
		StatusCode: 200,
	}, nil
}

func main() {
	lambda.Start(Handler)
}
