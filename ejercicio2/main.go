package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	_ "github.com/go-sql-driver/mysql"
)

type body struct {
	Message string `json:"message"`
}

type SensitiveData struct {
	Id          int    `json:"id"`
	OwnerRoleId int    `json:"owner_role_id"`
	Data        string `json:"data"`
}

// Handler is the Lambda function handler
// Our user's role id is 45
// curl --data "{\"role_id\":45}" http://localhost:3000/
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	log.Println("Lambda request", request.RequestContext.RequestID)
	params := request.Body
	log.Println(params)
	var requestBody map[string]interface{}
	err := json.Unmarshal([]byte(params), &requestBody)
	if err != nil {
		panic(err)
	}
	log.Println(requestBody["role_id"])
	dbHost := os.Getenv("DB_HOST")
	dbUsername := os.Getenv("DB_USERNAME")
	dbPassword := os.Getenv("DB_PW")
	dbSchema := os.Getenv("DB_SCHEMA")
	connectionString := fmt.Sprintf("%s:%s@tcp(%s:3306)/%s", dbUsername, dbPassword, dbHost, dbSchema)
	db, err := sql.Open("mysql", connectionString)

	if err != nil {
		fmt.Println(err.Error())
	} else {
		defer db.Close()
	}

	results, err := db.Query(fmt.Sprintf("SELECT * FROM sensitive_data where owner_role_id=%v", requestBody["role_id"]))
	if err != nil {
		panic(err.Error()) // proper error handling instead of panic in your app
	}
	var data []SensitiveData
	for results.Next() {
		var record SensitiveData
		err = results.Scan(&record.Id, &record.OwnerRoleId, &record.Data)
		if err != nil {
			panic(err.Error()) // proper error handling instead of panic in your app
		}
		data = append(data, record)
	}

	b, _ := json.Marshal(data)

	return events.APIGatewayProxyResponse{
		Body:       string(b),
		StatusCode: 200,
	}, nil
}

func main() {
	lambda.Start(Handler)
}
