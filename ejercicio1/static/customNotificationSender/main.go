package main

import (
	"fmt"
	"os"
)

func main() {
	argsWithoutProg := os.Args[1:]
	if len(argsWithoutProg) == 1 {
		fmt.Println("Sending a notification to " + argsWithoutProg[0])
	} else {
		fmt.Println("User to be notified was not selected")
	}
}
