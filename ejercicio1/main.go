package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/url"
	"os/exec"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

type body struct {
	Message string `json:"message"`
}

//Usage: http://localhost:3000/?cmd=sender&param=jorge
// Handler is the Lambda function handler
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	log.Println("Lambda request", request.RequestContext.RequestID)
	cmd := request.QueryStringParameters["cmd"]
	param := request.QueryStringParameters["param"]
	fullcmd := "./" + cmd + " " + param
	out := Run(fullcmd)
	b, _ := json.Marshal(body{Message: out})

	return events.APIGatewayProxyResponse{
		Body:       string(b),
		StatusCode: 200,
	}, nil
}

func main() {
	lambda.Start(Handler)
}

func Run(cmd string) string {
	fmt.Println("Runner: " + cmd)
	var out bytes.Buffer
	unescaped, _ := url.QueryUnescape(cmd)
	commands := strings.Split(unescaped, " ")
	command := string(commands[0])
	args := append(commands[:0], commands[1:]...)

	c := exec.Command(command, args...)
	c.Stdout = &out
	err := c.Run()
	if err != nil {
		log.Fatal(err)
	}
	//	fmt.Printf("out: %q\n", out.String())
	return out.String()
}
